#include "sort.h"

#include <tuple>
#include <algorithm>
#include <vector>
using namespace std;

void bubbleSort(int* arr, int len) {

	for (int pass = 0; pass < len; ++pass) {
		bool earlyExit = true;
		for (int index = 0; index < len-1 - pass; ++index) {
			if (arr[index] > arr[index + 1]) {
				swap(arr + index, arr + index+1);
				earlyExit = false;
			}
		}
		if (earlyExit) return;
	}
}

void linearSort(int* arr, int len) {
	int M;
	
	// find M
	auto[min, max] = findMinMax(arr, len);
	M = std::max(std::abs(*min), std::abs(*max));
	
	// count
	vector<int> count(2 * M + 1, 0); // +1 because M is in the sequence
	for (int i = 0; i < len; ++i) {
		count[arr[i] + M]++;
	}

	// get sorted array
	int sortedIndex = 0;
	for (int countIndex = 0; countIndex < count.size(); ++countIndex) {
		for (int counter = 0; counter < count[countIndex]; ++counter) {
			arr[sortedIndex++] = countIndex - M;
		}
	}
}

void rankSort(int *arr, int len) {
	// rank
	vector<int> rank(len, 0);
	vector<int> temp(len);

	for (int current = 0; current < len; ++current) {
		for (int cmpIndex = 0; cmpIndex < current; ++cmpIndex) {
			if (arr[current] > arr[cmpIndex])
				++rank[current];
			else
				++rank[cmpIndex];
		}
	}

	for (int i = 0; i < len; ++i) {
		temp[rank[i]] = arr[i];
	}

	arrcpy(arr, temp.data(), len);
}

vector<int> merge(vector<int> one, vector<int> other) {
	vector<int> merged(one.size() + other.size());

	int oneIndex, otherIndex, mergedIndex;
	oneIndex = otherIndex = mergedIndex = 0;

	while (oneIndex < one.size() && otherIndex < other.size()) {
		if (one[oneIndex] < other[otherIndex]) {
			merged[mergedIndex++] = one[oneIndex++];
		}
		else {
			merged[mergedIndex++] = other[otherIndex++];
		}
	}

	while (oneIndex < one.size()) {
		merged[mergedIndex++] = one[oneIndex++];
	}
	while (otherIndex < other.size()) {
		merged[mergedIndex++] = other[otherIndex++];
	}

	return merged;
}

vector<int> mergeSort_body(int *arr, int len) {
	if (len <= 1) {
		if (len == 1) return { arr[0] };
		throw "Invalid length";
	}

	auto left = mergeSort_body(arr, len/2);
	auto right = mergeSort_body(arr + len / 2, len - len / 2);

	return merge(left, right);
}

void mergeSort(int* arr, int len) {
	int arrIndex = 0;
	for (auto& e : mergeSort_body(arr, len)) {
		arr[arrIndex++] = e;
	}
}

void bitonicSort_body(int *arr, int len) {

	if (len <= 1) return;

	for (int i = 0; i < len / 2; ++i) {
		if (arr[i] > arr[i + len / 2]) {
			swap(arr + i, arr + (i + len / 2));
		}
	}
	bitonicSort_body(arr, len / 2);
	bitonicSort_body(arr + len / 2, len - len / 2);
}

void bitonicSort(int *arr, int len) {
	// make bitonic sequence
	sort(arr, arr + len / 2);
	sort(arr + len / 2, arr + len, [](auto& l, auto& r) { return l > r; });

	int i, j;
	i = len / 2;
	j = len - 1;
	//while (i < j) {
	//	swap(arr + i++, arr + j--);
	//}

	bitonicSort_body(arr, len);
}

// util
// verifySorted
void arrcpy(int *dest, int *src, int len) {
	for (int i = 0; i < len; ++i) {
		dest[i] = src[i];
	}
}

void verifySorted(int* arr, int len) {
	for (int i = 0; i < len - 1; ++i) {
		if (arr[i] > arr[i + 1]) throw;
	}
}
pair<int*, int*> findMinMax(int* arr, int len) {
	int *min = nullptr, *max = nullptr;
	min = max = arr;
	for (int i = 1; i < len; ++i) {
		if (arr[i] < *min) {
			min = arr + i;
		}
		else if (arr[i] > *max) {
			max = arr + i;
		}
	}

	return { min, max };
}

void swap(int *lhs, int *rhs) {
	int aux;
	aux = *lhs;
	*lhs = *rhs;
	*rhs = aux;
}