#pragma once

#include <utility>

void swap(int *, int *);
std::pair<int*, int*> findMinMax(int* arr, int len);

void bubbleSort(int* arr, int len);
void linearSort(int* arr, int len);
void mergeSort(int* arr, int len);
void rankSort(int *arr, int len);
void bitonicSort(int *arr, int len);

void verifySorted(int* arr, int len);
void arrcpy(int *dest, int *src, int len);