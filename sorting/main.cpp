#include "sort.h"
#include <vector>
#include <array>
#include <iostream>
#include <iomanip>
#include <string>
#include <map>
#include <random>
#include <cmath>
#include <chrono>
#include <ctime>
#include <cstring>
#include <fstream>
using namespace std;

void print(int *arr, int len);
vector<int> merge(vector<int> one, vector<int> other);

vector<int> getRandomList(int N, int min, int max) {
	// Seed with a real random value, if available
	std::random_device r;

	std::default_random_engine e1(r());
	std::uniform_int_distribution<int> uniform_dist(min, max);

	vector<int> ret(N);
	for (int i = 0; i < N; ++i) {
		ret[i] = uniform_dist(e1);
	}
	return ret;
}

enum Alg {
	MERGE,
	BUBBLE,
	LINEAR,
	RANK,
	BITONIC
};

const char* toStr(Alg alg) {
	static constexpr int len = 5;
	static const char* names[len] = { "MergeSort", "BubbleSort", "LinearSort", "RankSort", "BitonicSort" };
	if ((int)alg >= len || (int)alg < 0) throw;
	return names[(int)alg];
}

void (*callSort(Alg alg))(int*,int)  {
	static constexpr int len = 5;
	static void (*fn[len])(int*,int) = { mergeSort, bubbleSort, linearSort, rankSort, bitonicSort };
	if ((int)alg >= len || (int)alg < 0) throw;
	return fn[(int)alg];
}

void printUsage(const char* prog) {
	cout << "Usage: " << prog << "[OPTION] length\n"
		<< "OPTION:\n"
		<< "\t" << toStr(MERGE) << " - " << MERGE << "\n"
		<< "\t" << toStr(BUBBLE) << " - " << BUBBLE << "\n"
		<< "\t" << toStr(LINEAR) << " - " << LINEAR << "\n"
		<< "\t" << toStr(RANK) << " - " << RANK << "\n"
		<< "\t" << toStr(BITONIC) << " - " << BITONIC << "\n"
		;
}

constexpr size_t closestTo(size_t point) {
	size_t ret = 2;
	while (ret <= point) {
		ret *= 2;
	}
	return ret;
}

const char* outputFilePath = "sort_data.txt";

int main(int argc, const char* argv[]) {
	Alg alg{BITONIC};
	size_t len = 100000;

	if (argc > 1) {
		if (argc != 3) {
			printUsage(argv[0]);
			return 1;
		}
		alg = (Alg)(argv[1][0] - '0');
		len = strtol(argv[2], 0, 10);
		if (!len) {
			printUsage(argv[0]);
			return 2;
		}
	}
	cout << "Using: " << toStr(alg) << endl;

	vector<int> arr;
	if (BITONIC == alg) {
		len = closestTo(len);
	}
	arr = getRandomList(len, 1, 10);

	auto start = std::chrono::system_clock::now();
	callSort(alg)(&arr[0], arr.size());
	auto end = std::chrono::system_clock::now();

	// calc duration
	std::chrono::duration<double> elapsed_seconds = end - start;
	std::time_t end_time = std::chrono::system_clock::to_time_t(end);

	// Display results
	std::cout << "finished computation at " << std::ctime(&end_time)
		<< "elapsed time: " << elapsed_seconds.count() << "s\n";
	// Log
	ofstream out(string(toStr(alg)) + ".txt", std::ios_base::app);
	out << len << "," << elapsed_seconds.count() << "\n";

	verifySorted(&arr[0], arr.size());
	return 0;
}

void print(int *arr, int len) {
	for (int i = 0; i < len; ++i) {
		cout << arr[i] << " ";
	}
}